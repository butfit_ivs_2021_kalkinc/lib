/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib.math;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FactorialOperatorTest extends TestCase {

    private Expression expression0;
    private Expression expression1;
    private Expression expression1_3;
    private Expression expression1_8;
    private Expression expression2_04;
    private Expression expression2_97;
    private Expression expression3;
    private Expression expression4;
    private Expression expression13;
    private Expression expressionNeg3;
    private Expression expressionNeg4;
    private FactorialOperator operator;

    @Before
    public void setUp() {
        expression0 = mock(Expression.class);
        expression1 = mock(Expression.class);
        expression1_3 = mock(Expression.class);
        expression1_8 = mock(Expression.class);
        expression2_04 = mock(Expression.class);
        expression2_97 = mock(Expression.class);
        expression3 = mock(Expression.class);
        expression4 = mock(Expression.class);
        expression13 = mock(Expression.class);
        expressionNeg3 = mock(Expression.class);
        expressionNeg4 = mock(Expression.class);
        when(expression0.evaluate()).thenReturn(0d);
        when(expression1.evaluate()).thenReturn(1d);
        when(expression1_3.evaluate()).thenReturn(1.3d);
        when(expression1_8.evaluate()).thenReturn(1.8d);
        when(expression2_04.evaluate()).thenReturn(2.04d);
        when(expression2_97.evaluate()).thenReturn(2.97d);
        when(expression3.evaluate()).thenReturn(3d);
        when(expression4.evaluate()).thenReturn(4d);
        when(expression13.evaluate()).thenReturn(13d);
        when(expressionNeg3.evaluate()).thenReturn(-3d);
        when(expressionNeg4.evaluate()).thenReturn(-4d);
        operator = new FactorialOperator();
    }


    public void test0Expression() {
            assertEquals(operator.apply(new ArrayList<>()), 0d);
    }

    public void testAnythingFactorial() {
        assertEquals(operator.apply(Collections.singletonList(expression0)), 1d);
        assertEquals(operator.apply(Collections.singletonList(expression1)), 1d);
        assertEquals(operator.apply(Collections.singletonList(expression3)), 6d);
        assertEquals(operator.apply(Collections.singletonList(expression4)), 24d);
        assertEquals(operator.apply(Collections.singletonList(expression13)), 6227020800d);
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Collections.singletonList(expressionNeg3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Collections.singletonList(expressionNeg4)));
    }

    public void testNotWholeNumberShouldNotPass() {
        Assert.assertThrows(IntegerExpressionExpectedException.class, () -> operator.apply(Collections.singletonList(expression1_3)));
        Assert.assertThrows(IntegerExpressionExpectedException.class, () -> operator.apply(Collections.singletonList(expression1_8)));
    }

    public void testNotWholeNumberShouldPass() {
        assertEquals(operator.apply(Collections.singletonList(expression2_04)), 2d);
        assertEquals(operator.apply(Collections.singletonList(expression2_97)), 6d);
    }

    public void test2AndMoreExpressions() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expression1)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression4, expressionNeg3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression1, expression4, expressionNeg3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expression1, expressionNeg3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expression1, expression4, expressionNeg3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression1_8, expression0, expressionNeg4, expression1_3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression1, expression1_8, expressionNeg4, expression2_04, expressionNeg4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression1, expression0, expressionNeg4, expression1_3, expression0)));
    }
}