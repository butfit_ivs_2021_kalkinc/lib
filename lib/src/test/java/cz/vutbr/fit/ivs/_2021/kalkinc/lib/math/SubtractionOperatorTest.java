/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib.math;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class SubtractionOperatorTest {

    private Expression expression0;
    private Expression expression6;
    private Expression expressionNeg6;
    private SubtractionOperator operator;

    @Before
    public void SetUp() {
        expression0 = mock(Expression.class);
        expression6 = mock(Expression.class);
        expressionNeg6 = mock (Expression.class);
        when(expression0.evaluate()).thenReturn(0d);
        when(expression6.evaluate()).thenReturn(6d);
        when(expressionNeg6.evaluate()).thenReturn(-6d);
        operator = new SubtractionOperator();
    }

    @Test
    public void test0from0Subtraction() {
        assertEquals(operator.apply(Arrays.asList(expression0, expression0)), 0d, 0d);
    }

    @Test
    public void test0fromPosSubtraction() {
        assertEquals(operator.apply(Arrays.asList(expression6, expression0)), 6d,0d);
    }

    @Test
    public void test0fromNegSubtraction() {
        assertEquals(operator.apply(Arrays.asList(expressionNeg6, expression0)), -6d,0d);
    }

    @Test
    public void testPosFrom0Subtraction() {
        assertEquals(operator.apply(Arrays.asList(expression0, expression6)), -6d, 0d);
    }

    @Test
    public void testNegFrom0Subtraction() {
        assertEquals(operator.apply(Arrays.asList(expression0, expressionNeg6)), 6d, 0d);
    }

    @Test
    public void testSingleOperatorSubtraction() {
        assertEquals(operator.apply(Collections.singletonList(expression0)), 0d, 0d);
        assertEquals(operator.apply(Collections.singletonList(expression6)), 6d, 0d);
        assertEquals(operator.apply(Collections.singletonList(expressionNeg6)), -6d, 0d);
    }

    @Test
    public void testMultipleOperatorSubtraction() {
        assertEquals(operator.apply(Arrays.asList(expression6, expression6, expression0)), 0d, 0d);
        assertEquals(operator.apply(Arrays.asList(expression6, expressionNeg6, expression0)), 12d, 0d);
        assertEquals(operator.apply(Arrays.asList(expressionNeg6, expression6, expression0)), -12d, 0d);
        assertEquals(operator.apply(Arrays.asList(expressionNeg6, expressionNeg6, expression0)), 0d,0d);
        assertEquals(operator.apply(Arrays.asList(expression6, expression6, expression6)), -6, 0d);
        assertEquals(operator.apply(Arrays.asList(expression6, expressionNeg6, expression6)), 6d, 0d);
        assertEquals(operator.apply(Arrays.asList(expressionNeg6, expression6, expression6)), -18d, 0d);
        assertEquals(operator.apply(Arrays.asList(expressionNeg6, expressionNeg6, expressionNeg6)), 6d, 0d);
    }
}
