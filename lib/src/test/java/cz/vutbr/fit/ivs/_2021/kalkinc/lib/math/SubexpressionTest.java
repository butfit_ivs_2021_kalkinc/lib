/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib.math;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SubexpressionTest extends TestCase {

    private Expression expression0;
    private Expression expression1;
    private Expression expression1_2;
    private Expression expression1_97;
    private Expression expression2;
    private Expression expression3;
    private Expression expression4;
    private Expression expression7;
    private Expression expression8;
    private Expression expression16;
    private Expression expression256;
    private Expression expressionNeg1;
    private Expression expressionNeg2;
    private Expression expressionNeg3;
    private Expression expressionNeg4;
    private Expression expressionNeg7;
    private Expression expressionNeg8;
    private Expression expressionNeg16;
    private Operator Addition;
    private Operator Subtraction;
    private Operator Multiplication;
    private Operator Division;
    private Operator Power;
    private Operator Root;
    private Operator Logarithm;
    private Operator Factorial;

    @Before
    public void setUp() {
        expression0 = mock(Expression.class);
        expression1 = mock(Expression.class);
        expression1_2 = mock(Expression.class);
        expression1_97 = mock(Expression.class);
        expression2 = mock(Expression.class);
        expression3 = mock(Expression.class);
        expression4 = mock(Expression.class);
        expression7 = mock(Expression.class);
        expression8 = mock(Expression.class);
        expression16 = mock(Expression.class);
        expression256 = mock(Expression.class);
        expressionNeg1 = mock(Expression.class);
        expressionNeg2 = mock(Expression.class);
        expressionNeg3 = mock(Expression.class);
        expressionNeg4 = mock(Expression.class);
        expressionNeg7 = mock(Expression.class);
        expressionNeg8 = mock(Expression.class);
        expressionNeg16 = mock(Expression.class);
        when(expression0.evaluate()).thenReturn(0d);
        when(expression1.evaluate()).thenReturn(1d);
        when(expression1_2.evaluate()).thenReturn(1.2d);
        when(expression1_97.evaluate()).thenReturn(1.97d);
        when(expression2.evaluate()).thenReturn(2d);
        when(expression3.evaluate()).thenReturn(3d);
        when(expression4.evaluate()).thenReturn(4d);
        when(expression7.evaluate()).thenReturn(7d);
        when(expression8.evaluate()).thenReturn(8d);
        when(expression16.evaluate()).thenReturn(16d);
        when(expression256.evaluate()).thenReturn(256d);
        when(expressionNeg1.evaluate()).thenReturn(-1d);
        when(expressionNeg2.evaluate()).thenReturn(-2d);
        when(expressionNeg3.evaluate()).thenReturn(-3d);
        when(expressionNeg4.evaluate()).thenReturn(-4d);
        when(expressionNeg7.evaluate()).thenReturn(-7d);
        when(expressionNeg8.evaluate()).thenReturn(-8d);
        when(expressionNeg16.evaluate()).thenReturn(-16d);
        Addition = new AdditionOperator();
        //Subtraction = new SubtractionOperator();
        Multiplication = new MultiplicationOperator();
        Division = new DivisionOperator();
        Power = new PowerOperator();
        Root = new RootOperator();
        Logarithm = new LogarithmOperator();
        Factorial = new FactorialOperator();
    }

    public void testAddition() {
        Subexpression addition1 = new Subexpression(Addition, Collections.singletonList(expression3));
        assertEquals(addition1.evaluate(), 3d);
        Subexpression addition2 = new Subexpression(Addition, Arrays.asList(expression4, expression8));
        assertEquals(addition2.evaluate(), 12d);
        Subexpression addition3 = new Subexpression(Addition, Arrays.asList(expression1, expression2, expression3));
        assertEquals(addition3.evaluate(), 6d);
        Subexpression addition4 = new Subexpression(Addition, Arrays.asList(expression256, expression16, expression4, expression4));
        assertEquals(addition4.evaluate(), 280d);
        Subexpression addition5 = new Subexpression(Addition, Arrays.asList(expression256, expressionNeg16, expressionNeg4, expressionNeg1));
        assertEquals(addition5.evaluate(), 235d);
    }
    /*
    public void testSubtraction() {
        Subexpression subtraction1 = new Subexpression(Subtraction, Collections.singletonList(expression3));
        assertEquals(subtraction1.evaluate(), 3d);
        Subexpression subtraction2 = new Subexpression(Subtraction, Arrays.asList(expression4, expression8));
        assertEquals(subtraction2.evaluate(), -4d);
    }
     */
    public void testMultiplication() {
        Subexpression multiplication1 = new Subexpression(Multiplication, Collections.singletonList(expression7));
        assertEquals(multiplication1.evaluate(), 7d);
        Subexpression multiplication2 = new Subexpression(Multiplication, Arrays.asList(expression0, expression8));
        assertEquals(multiplication2.evaluate(), 0d);
        Subexpression multiplication3 = new Subexpression(Multiplication, Arrays.asList(expressionNeg1, expression2, expressionNeg3));
        assertEquals(multiplication3.evaluate(), 6d);
        Subexpression multiplication4 = new Subexpression(Multiplication, Arrays.asList(expression16, expression16, expression4, expression4));
        assertEquals(multiplication4.evaluate(), 4096d);
        Subexpression multiplication5 = new Subexpression(Multiplication, Arrays.asList(expression256, expressionNeg4, expressionNeg4, expressionNeg1));
        assertEquals(multiplication5.evaluate(), -4096d);
    }

    public void testDivision() {
        Subexpression division1 = new Subexpression(Division, Collections.singletonList(expressionNeg3));
        assertEquals(division1.evaluate(), -3d);
        Subexpression division2 = new Subexpression(Division, Arrays.asList(expressionNeg7, expression0));
        assertEquals(division2.evaluate(), Double.NEGATIVE_INFINITY);
        Subexpression division3 = new Subexpression(Division, Arrays.asList(expressionNeg1, expressionNeg3));
        assertEquals(division3.evaluate(), 1/3d);
        Subexpression division4 = new Subexpression(Division, Arrays.asList(expression16, expressionNeg4));
        assertEquals(division4.evaluate(), -4d);
        Subexpression division5 = new Subexpression(Division, Arrays.asList(expression256, expression7));
        assertEquals(division5.evaluate(), 256/7d);
        Subexpression division6 = new Subexpression(Division, Arrays.asList(expression256, expression7, expression4));
        Assert.assertThrows(IllegalArgumentException.class, division6::evaluate);
    }

    public void testPower() {
        Subexpression power1 = new Subexpression(Power, Collections.singletonList(expressionNeg3));
        assertEquals(power1.evaluate(), -3d);
        Subexpression power2 = new Subexpression(Power, Arrays.asList(expressionNeg7, expression0));
        assertEquals(power2.evaluate(), 1d);
        Subexpression power3 = new Subexpression(Power, Arrays.asList(expressionNeg1, expressionNeg3));
        assertEquals(power3.evaluate(), -1d);
        Subexpression power4 = new Subexpression(Power, Arrays.asList(expression16, expressionNeg2));
        assertEquals(power4.evaluate(), 1/256d);
        Subexpression power5 = new Subexpression(Power, Arrays.asList(expression16, expression2));
        assertEquals(power5.evaluate(), 256d);
        Subexpression power6 = new Subexpression(Power, Arrays.asList(expression256, expression7, expression4));
        Assert.assertThrows(IllegalArgumentException.class, power6::evaluate);
    }

    public void testRoot() {
        Subexpression root1 = new Subexpression(Root, Collections.singletonList(expression4));
        assertEquals(root1.evaluate(), 2d);
        Subexpression root2 = new Subexpression(Root, Arrays.asList(expression7, expression0));
        assertEquals(root2.evaluate(), 0d);
        Subexpression root3 = new Subexpression(Root, Arrays.asList(expression3, expressionNeg8));
        assertEquals(root3.evaluate(), -2d);
        Subexpression root4 = new Subexpression(Root, Arrays.asList(expressionNeg4, expression256));
        Assert.assertThrows(IllegalArgumentException.class, root4::evaluate);
        Subexpression root5 = new Subexpression(Root, Arrays.asList(expression4, expression256));
        assertEquals(root5.evaluate(), 4d);
        Subexpression root6 = new Subexpression(Root, Arrays.asList(expression0, expression4));
        Assert.assertThrows(IllegalArgumentException.class, root6::evaluate);
    }

    public void testLogarithm() {
        Subexpression logarithm1 = new Subexpression(Logarithm, Collections.singletonList(expression16));
        assertEquals(logarithm1.evaluate(), 1.2041199826559248d);
        Subexpression logarithm2 = new Subexpression(Logarithm, Collections.singletonList(expressionNeg2));
        Assert.assertThrows(IllegalArgumentException.class, logarithm2::evaluate);
        Subexpression logarithm3 = new Subexpression(Logarithm, Arrays.asList(expression2, expression256));
        assertEquals(logarithm3.evaluate(), 8d);
        Subexpression logarithm4 = new Subexpression(Logarithm, Arrays.asList(expressionNeg4, expression256));
        Assert.assertThrows(IllegalArgumentException.class, logarithm4::evaluate);
        Subexpression logarithm5 = new Subexpression(Logarithm, Arrays.asList(expression4, expression4));
        assertEquals(logarithm5.evaluate(), 1d);
        Subexpression logarithm6 = new Subexpression(Logarithm, Arrays.asList(expression0, expression4, expression1));
        Assert.assertThrows(IllegalArgumentException.class, logarithm6::evaluate);
    }

    public void testFactorial() {
        Subexpression factorial1 = new Subexpression(Factorial, Collections.singletonList(expression7));
        assertEquals(factorial1.evaluate(), 5040d);
        Subexpression factorial2 = new Subexpression(Factorial, Collections.singletonList(expression1_2));
        Assert.assertThrows(IntegerExpressionExpectedException.class, factorial2::evaluate);
        Subexpression factorial3 = new Subexpression(Factorial, Collections.singletonList(expression1_97));
        assertEquals(factorial3.evaluate(), 2d);
        Subexpression factorial4 = new Subexpression(Factorial, Collections.singletonList(expressionNeg4));
        Assert.assertThrows(IllegalArgumentException.class, factorial4::evaluate);
        Subexpression factorial5 = new Subexpression(Factorial, Collections.singletonList(expression4));
        assertEquals(factorial5.evaluate(), 24d);
        Subexpression factorial6 = new Subexpression(Factorial, Arrays.asList(expression0, expression4, expression1));
        Assert.assertThrows(IllegalArgumentException.class, factorial6::evaluate);
    }
}