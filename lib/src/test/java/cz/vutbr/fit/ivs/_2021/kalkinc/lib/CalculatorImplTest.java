/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib;

import cz.vutbr.fit.ivs._2021.kalkinc.lib.parser.ParserException;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class CalculatorImplTest {

    private Token token1;
    private Token token2;
    private Token tokenPlus;
    private Token tokenAns;
    private Token tokenM;
    private Token tokenMPlus;
    private Token tokenMMinus;

    @Before
    public void setUp() {
        token1 = mock (Token.class);
        token2 = mock (Token.class);
        tokenPlus = mock (Token.class);
        tokenAns = mock (Token.class);
        tokenM = mock (Token.class);
        tokenMPlus = mock(Token.class);
        tokenMMinus = mock(Token.class);
        when(token1.getValue()).thenReturn("1");
        when(token2.getValue()).thenReturn("2");
        when(tokenPlus.getValue()).thenReturn("+");
        when(tokenAns.getValue()).thenReturn("Ans");
        when(tokenM.getValue()).thenReturn("M");
        when(tokenMMinus.getValue()).thenReturn("M-");
        when(tokenMPlus.getValue()).thenReturn("M+");
    }

    @Test
    public void testOnePlusOne() throws ParserException {
        List<Token> tokenList = new ArrayList<>();
        tokenList.add(token1);
        tokenList.add(tokenPlus);
        tokenList.add(token1);
        CalculatorImpl test = new CalculatorImpl();
        test.setExpression(tokenList);
        assertEquals(test.getResult(), 2d, 0d);
    }

    @Test
    public void testAnsPlusOne() throws ParserException {
        List<Token> tokenList = new ArrayList<>();
        tokenList.add(token1);
        tokenList.add(tokenPlus);
        tokenList.add(token1);
        CalculatorImpl test = new CalculatorImpl();
        test.setExpression(tokenList);
        List<Token> tokenList2 = new ArrayList<>();
        tokenList2.add(tokenAns);
        tokenList2.add(tokenPlus);
        tokenList2.add(token1);
        test.setExpression(tokenList2);
        assertEquals(test.getResult(), 3d, 0d);
    }

    @Test
    public void testMPlusPlusOne() throws ParserException {
        List<Token> tokenList = new ArrayList<>();
        tokenList.add(token1);
        tokenList.add(tokenPlus);
        tokenList.add(token1);
        CalculatorImpl test = new CalculatorImpl();
        test.setExpression(tokenList);
        List<Token> tokenList2 = new ArrayList<>();
        tokenList2.add(tokenMPlus);
        test.setExpression(tokenList2);
        List<Token> tokenList3 = new ArrayList<>();
        tokenList3.add(tokenM);
        tokenList3.add(tokenPlus);
        tokenList3.add(token1);
        test.setExpression(tokenList3);
        assertEquals(test.getResult(), 3d, 0d);
    }

    @Test
    public void testMPlusPlusOne2() throws ParserException {
        List<Token> tokenList = new ArrayList<>();
        tokenList.add(token1);
        tokenList.add(tokenPlus);
        tokenList.add(token1);
        CalculatorImpl test = new CalculatorImpl();
        test.setExpression(tokenList);
        List<Token> tokenList2 = new ArrayList<>();
        tokenList.add(tokenAns);
        tokenList2.add(tokenMPlus);
        test.setExpression(tokenList2);
        List<Token> tokenList3 = new ArrayList<>();
        tokenList3.add(tokenM);
        tokenList3.add(tokenPlus);
        tokenList3.add(token1);
        test.setExpression(tokenList3);
        assertEquals(test.getResult(), 3d, 0d);
    }

    @Test
    public void testMMinusPlusOne() throws ParserException {
        List<Token> tokenList = new ArrayList<>();
        tokenList.add(token1);
        tokenList.add(tokenPlus);
        tokenList.add(token1);
        CalculatorImpl test = new CalculatorImpl();
        test.setExpression(tokenList);
        List<Token> tokenList2 = new ArrayList<>();
        tokenList2.add(tokenMMinus);
        test.setExpression(tokenList2);
        List<Token> tokenList3 = new ArrayList<>();
        tokenList3.add(tokenM);
        tokenList3.add(tokenPlus);
        tokenList3.add(token1);
        test.setExpression(tokenList3);
        assertEquals(test.getResult(), -1d, 0d);
    }

    @Test
    public void testMMinusPlusOne2() throws ParserException {
        List<Token> tokenList = new ArrayList<>();
        tokenList.add(token1);
        tokenList.add(tokenPlus);
        tokenList.add(token1);
        CalculatorImpl test = new CalculatorImpl();
        test.setExpression(tokenList);
        List<Token> tokenList2 = new ArrayList<>();
        tokenList2.add(tokenAns);
        tokenList2.add(tokenMMinus);
        test.setExpression(tokenList2);
        List<Token> tokenList3 = new ArrayList<>();
        tokenList3.add(tokenM);
        tokenList3.add(tokenPlus);
        tokenList3.add(token1);
        test.setExpression(tokenList3);
        assertEquals(test.getResult(), -1d, 0d);
    }

    @Test
    public void testMMplus() throws ParserException {
        List<Token> tokenList = new ArrayList<>();
        tokenList.add(token2);
        tokenList.add(tokenPlus);
        tokenList.add(token2);
        CalculatorImpl test = new CalculatorImpl();
        test.setExpression(tokenList);
        List<Token> tokenList2 = new ArrayList<>();
        tokenList2.add(tokenMPlus);
        test.setExpression(tokenList2);
        List<Token> tokenList3 = new ArrayList<>();
        tokenList3.add(tokenM);
        tokenList3.add(tokenMPlus);
        test.setExpression(tokenList3);
        assertEquals(test.getResult(), 8d, 0d);
    }

    @Test
    public void testMMminus() throws ParserException {
        List<Token> tokenList = new ArrayList<>();
        tokenList.add(token2);
        tokenList.add(tokenPlus);
        tokenList.add(token2);
        CalculatorImpl test = new CalculatorImpl();
        test.setExpression(tokenList);
        List<Token> tokenList2 = new ArrayList<>();
        tokenList2.add(tokenMPlus);
        test.setExpression(tokenList2);
        List<Token> tokenList3 = new ArrayList<>();
        tokenList3.add(tokenM);
        tokenList3.add(tokenMMinus);
        test.setExpression(tokenList3);
        assertEquals(test.getResult(), 0d, 0d);
    }
}
