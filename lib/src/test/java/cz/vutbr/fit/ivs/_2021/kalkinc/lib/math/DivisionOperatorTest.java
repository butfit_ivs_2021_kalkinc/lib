/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib.math;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.*;

public class DivisionOperatorTest extends TestCase {

    private Expression expression0;
    private Expression expression1;
    private Expression expression4;
    private Expression expression16;
    private Expression expressionNeg16;
    private Expression expressionNeg4;
    private DivisionOperator operator;

    @Before
    public void setUp() {
        expression0 = mock(Expression.class);
        expression1 = mock(Expression.class);
        expression4 = mock(Expression.class);
        expression16 = mock(Expression.class);
        expressionNeg4 = mock(Expression.class);
        expressionNeg16 = mock(Expression.class);
        when(expression0.evaluate()).thenReturn(0d);
        when(expression1.evaluate()).thenReturn(1d);
        when(expression4.evaluate()).thenReturn(4d);
        when(expression16.evaluate()).thenReturn(16d);
        when(expressionNeg4.evaluate()).thenReturn(-4d);
        when(expressionNeg16.evaluate()).thenReturn(-16d);
        operator = new DivisionOperator();
    }

    public void testNoExpressions() {
        assertEquals(operator.apply(new ArrayList<>()), 0d);
    }

    public void test1Expression() {
        assertEquals(operator.apply(Collections.singletonList(expression0)), 0d);
        assertEquals(operator.apply(Collections.singletonList(expression4)), 4d);
        assertEquals(operator.apply(Collections.singletonList(expressionNeg16)), -16d);
    }

    public void test0DividedBy0() {
        assertEquals(operator.apply(Arrays.asList(expression0, expression0)), Double.NaN);
    }

    public void test0DividedByAnything() {
        assertEquals(operator.apply(Arrays.asList(expression0, expression1)), 0d);
        assertEquals(operator.apply(Arrays.asList(expression0, expression4)), 0d);
        assertEquals(operator.apply(Arrays.asList(expression0, expression16)), 0d);
        assertEquals(operator.apply(Arrays.asList(expression0, expressionNeg4)), -0d);
        assertEquals(operator.apply(Arrays.asList(expression0, expressionNeg16)), -0d);
    }

    public void testAnythingDividedBy0() {
        assertEquals(operator.apply(Arrays.asList(expression1, expression0)), Double.POSITIVE_INFINITY);
        assertEquals(operator.apply(Arrays.asList(expression4, expression0)), Double.POSITIVE_INFINITY);
        assertEquals(operator.apply(Arrays.asList(expression16, expression0)), Double.POSITIVE_INFINITY);
        assertEquals(operator.apply(Arrays.asList(expressionNeg4, expression0)), Double.NEGATIVE_INFINITY);
        assertEquals(operator.apply(Arrays.asList(expressionNeg16, expression0)), Double.NEGATIVE_INFINITY);
    }

    public void testPositiveDividedByPositive() {
        assertEquals(operator.apply(Arrays.asList(expression1, expression1)), 1d);
        assertEquals(operator.apply(Arrays.asList(expression16, expression4)), 4d);
        assertEquals(operator.apply(Arrays.asList(expression1, expression16)), 1/16d);
    }

    public void testPositiveDividedByNegative() {
        assertEquals(operator.apply(Arrays.asList(expression1, expressionNeg4)), -1/4d);
        assertEquals(operator.apply(Arrays.asList(expression4, expressionNeg16)), -1/4d);
        assertEquals(operator.apply(Arrays.asList(expression16, expressionNeg4)), -4d);
    }

    public void testNegativeDividedByPositive() {
        assertEquals(operator.apply(Arrays.asList(expressionNeg4, expression1)), -4d);
        assertEquals(operator.apply(Arrays.asList(expressionNeg4, expression16)), -1/4d);
        assertEquals(operator.apply(Arrays.asList(expressionNeg16, expression4)), -4d);
    }

    public void testNegativeDividedByNegative() {
        assertEquals(operator.apply(Arrays.asList(expressionNeg4, expressionNeg4)), 1d);
        assertEquals(operator.apply(Arrays.asList(expressionNeg4, expressionNeg16)), 1/4d);
        assertEquals(operator.apply(Arrays.asList(expressionNeg16, expressionNeg4)), 4d);
        assertEquals(operator.apply(Arrays.asList(expressionNeg16, expressionNeg16)), 1d);
    }
    public void test3Expressions() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expression1, expression4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg16, expression0, expressionNeg4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression1, expressionNeg16, expressionNeg4)));

    }
    public void testEvenMoreExpressions() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expression1, expression4, expressionNeg4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg16, expression0, expressionNeg4, expression16)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression1, expressionNeg16, expressionNeg4, expressionNeg16, expressionNeg4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression1, expressionNeg16, expressionNeg4, expression16, expression0)));
    }
}
