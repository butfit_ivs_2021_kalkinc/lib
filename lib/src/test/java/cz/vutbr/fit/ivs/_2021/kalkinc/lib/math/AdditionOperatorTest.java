/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib.math;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class AdditionOperatorTest {

    private Expression expression0;
    private Expression expression4;
    private Expression expressionNeg4;
    private AdditionOperator operator;

    @Before
    public void setUp() {
        expression0 = mock(Expression.class);
        expression4 = mock(Expression.class);
        expressionNeg4 = mock(Expression.class);
        when(expression0.evaluate()).thenReturn(0d);
        when(expression4.evaluate()).thenReturn(4d);
        when(expressionNeg4.evaluate()).thenReturn(-4d);
        operator = new AdditionOperator();
    }

    @Test
    public void test0To0Addition() {
        assertEquals(operator.apply(Arrays.asList(expression0, expression0)), 0d, 0d);
    }

    @Test
    public void test0ToPosAddition() {
        assertEquals(operator.apply(Arrays.asList(expression0, expression4)), 4d, 0d);
        assertEquals(operator.apply(Arrays.asList(expression4, expression0)), 4d, 0d);
    }

    @Test
    public void test0ToNegAddition() {
        assertEquals(operator.apply(Arrays.asList(expression0, expressionNeg4)), -4d, 0d);
        assertEquals(operator.apply(Arrays.asList(expressionNeg4, expression0)), -4d, 0d);
    }

    @Test
    public void testPosToNegAddition() {
        assertEquals(operator.apply(Arrays.asList(expression4, expressionNeg4)), 0d, 0d);
        assertEquals(operator.apply(Arrays.asList(expressionNeg4, expression4)), 0d, 0d);
    }

    @Test
    public void testSingleOperandAddition() {
        assertEquals(operator.apply(Collections.singletonList(expression0)), 0d, 0d);
        assertEquals(operator.apply(Collections.singletonList(expression4)), 4d, 0d);
        assertEquals(operator.apply(Collections.singletonList(expressionNeg4)), -4d, 0d);
    }

    @Test
    public void testMultipleOperatorAddition() {
        assertEquals(operator.apply(Arrays.asList(expressionNeg4, expressionNeg4, expression0)), -8d, 0d);
        assertEquals(operator.apply(Arrays.asList(expressionNeg4, expression4, expression0)), 0d, 0d);
        assertEquals(operator.apply(Arrays.asList(expressionNeg4, expression4, expression4)), 4d, 0d);
        assertEquals(operator.apply(Arrays.asList(expression4, expression4, expression4)), 12d, 0d);
    }
}
