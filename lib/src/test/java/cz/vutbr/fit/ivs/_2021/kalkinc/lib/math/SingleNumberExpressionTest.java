/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib.math;

import org.junit.Test;

import static org.junit.Assert.*;

public class SingleNumberExpressionTest {

    @Test
    public void basicTests(){
        SingleNumberExpression expression1 = new SingleNumberExpression(2.3d);
        assertEquals(expression1.evaluate(), 2.3d,0.0001d);
        SingleNumberExpression expression2 = new SingleNumberExpression(-2.3d);
        assertEquals(expression2.evaluate(), -2.3d,0.0001d);
        SingleNumberExpression expression3 = new SingleNumberExpression(16.8d);
        assertEquals(expression3.evaluate(), 16.8d,0.0001d);
        SingleNumberExpression expression4 = new SingleNumberExpression(-16.8d);
        assertEquals(expression4.evaluate(), -16.8d,0.0001d);
        SingleNumberExpression expression5 = new SingleNumberExpression(0d);
        assertEquals(expression5.evaluate(), 0d,0.0001d);
    }

}
