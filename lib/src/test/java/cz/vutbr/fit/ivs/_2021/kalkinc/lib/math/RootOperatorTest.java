/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib.math;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.*;

public class RootOperatorTest extends TestCase {

    private Expression expression0;
    private Expression expression1;
    private Expression expression3;
    private Expression expression4;
    private Expression expression16;
    private Expression expressionNeg2;
    private Expression expressionNeg3;
    private Expression expressionNeg4;
    private Expression expressionNeg8;
    private Expression expressionNeg16;
    private RootOperator operator;

    @Before
    public void setUp() {
        expression0 = mock(Expression.class);
        expression1 = mock(Expression.class);
        expression3  = mock(Expression.class);
        expression4 = mock(Expression.class);
        expression16 = mock(Expression.class);
        expressionNeg2 = mock(Expression.class);
        expressionNeg3 = mock(Expression.class);
        expressionNeg4 = mock(Expression.class);
        expressionNeg8 = mock(Expression.class);
        expressionNeg16 = mock(Expression.class);
        when(expression0.evaluate()).thenReturn(0d);
        when(expression1.evaluate()).thenReturn(1d);
        when(expression3.evaluate()).thenReturn(3d);
        when(expression4.evaluate()).thenReturn(4d);
        when(expression16.evaluate()).thenReturn(16d);
        when(expressionNeg2.evaluate()).thenReturn(-2d);
        when(expressionNeg3.evaluate()).thenReturn(-3d);
        when(expressionNeg4.evaluate()).thenReturn(-4d);
        when(expressionNeg8.evaluate()).thenReturn(-8d);
        when(expressionNeg16.evaluate()).thenReturn(-16d);
        operator = new RootOperator();
    }

    public void testNoExpressions() {
        assertEquals(operator.apply(new ArrayList<>()), 0d);
    }

    public void test1Expression() {
        assertEquals(operator.apply(Collections.singletonList(expression0)), 0d);
        assertEquals(operator.apply(Collections.singletonList(expression1)), 1d);
        assertEquals(operator.apply(Collections.singletonList(expression16)), 4d);
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Collections.singletonList(expressionNeg3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Collections.singletonList(expressionNeg8)));
    }

    public void test0Root0() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expression0)));
    }

    public void test0RootAnything() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expression1)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expression4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expression16)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expressionNeg3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expressionNeg4)));
    }

    public void testPositiveRoot0() {
        assertEquals(operator.apply(Arrays.asList(expression1, expression0)), 0d);
        assertEquals(operator.apply(Arrays.asList(expression4, expression0)), 0d);
        assertEquals(operator.apply(Arrays.asList(expression16, expression0)), 0d);
    }

    public void testNegativeRoot0() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg2, expression0)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg3, expression0)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg4, expression0)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg16, expression0)));
    }

    public void testPositiveRootPositive() {
        assertEquals(operator.apply(Arrays.asList(expression1, expression4)), 4d);
        assertEquals(operator.apply(Arrays.asList(expression4, expression1)), 1d);
        assertEquals(operator.apply(Arrays.asList(expression4, expression4)), 1.4142135623730951d);
        assertEquals(operator.apply(Arrays.asList(expression4, expression16)), 2d);
        assertEquals(operator.apply(Arrays.asList(expression16, expression1)), 1d);
    }

    public void testPositiveRootNegative() {
        assertEquals(operator.apply(Arrays.asList(expression1, expressionNeg4)), -4d);
        assertEquals(operator.apply(Arrays.asList(expression3, expressionNeg8)), -2d);
        assertEquals(operator.apply(Arrays.asList(expression3, expressionNeg16)), -2.5198420997897464d);
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression4, expressionNeg4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression16, expressionNeg16)));
    }

    public void testNegativeRootPositive() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg2, expression3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg3, expression1)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg4, expression16)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg8, expression4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg16, expression3)));
    }

    public void testNegativeRootNegative() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg2, expressionNeg4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg2, expressionNeg3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg3, expressionNeg4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg4, expressionNeg2)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg8, expressionNeg4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg16, expressionNeg3)));
    }
    public void test3Expressions() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg2, expression4, expressionNeg4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg2, expressionNeg3, expression0)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg4, expression4, expression4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression16, expressionNeg8, expression1)));
    }
    public void testEvenMoreExpressions() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg2, expression4, expressionNeg4, expression0)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg2, expressionNeg3, expression0, expression16)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg4, expression4, expression4, expressionNeg4, expression4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression16, expressionNeg8, expression1, expressionNeg2, expression4)));
    }
}
