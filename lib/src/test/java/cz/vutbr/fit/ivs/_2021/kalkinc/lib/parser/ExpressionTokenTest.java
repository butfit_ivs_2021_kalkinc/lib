/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib.parser;

import cz.vutbr.fit.ivs._2021.kalkinc.lib.Token;
import cz.vutbr.fit.ivs._2021.kalkinc.lib.TokenImpl;
import cz.vutbr.fit.ivs._2021.kalkinc.lib.math.Expression;
import junit.framework.TestCase;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ExpressionTokenTest extends TestCase {
    Expression expressionNeg4;
    Expression expression8;
    Token tokenLeftParen;
    Token token0;
    public void setUp() throws Exception {
        super.setUp();
        expressionNeg4 = mock(Expression.class);
        when(expressionNeg4.evaluate()).thenReturn(-4d);
        expression8 = mock(Expression.class);
        when(expression8.evaluate()).thenReturn(8d);
        tokenLeftParen = new TokenImpl("(");
        token0 = new TokenImpl("0");
    }

    public void testGetExpression() {
        assertNull(new ExpressionToken(token0).getExpression());
        assertEquals(new ExpressionToken(expression8).getExpression(), expression8);
        assertEquals(new ExpressionToken(expressionNeg4, tokenLeftParen).getExpression(), expressionNeg4);
    }

    public void testGetToken() {
        assertEquals(new ExpressionToken(token0).getToken(), token0);
        assertNull(new ExpressionToken(expression8).getToken());
        assertEquals(new ExpressionToken(expressionNeg4, tokenLeftParen).getToken(), tokenLeftParen);
    }

    public void testIsExpression() {
        assertFalse(new ExpressionToken(token0).isExpression());
        assertTrue(new ExpressionToken(expression8).isExpression());
        assertTrue(new ExpressionToken(expressionNeg4, tokenLeftParen).isExpression());
    }
}