package cz.vutbr.fit.ivs._2021.kalkinc.lib.parser;

import cz.vutbr.fit.ivs._2021.kalkinc.lib.TokenImpl;
import junit.framework.TestCase;

public class TokenParserTest extends TestCase {

    public void testParseToExpression() throws ParserException {
        assertEquals(120f, TokenParser.parseToExpression(new TokenImpl("5"), new TokenImpl("!")).evaluate(), 0.0001d);
        assertEquals(11f, TokenParser.parseToExpression(new TokenImpl("5"), new TokenImpl("+"), new TokenImpl("6")).evaluate(), 0.0001d);
        assertEquals(30f, TokenParser.parseToExpression(new TokenImpl("5"), new TokenImpl("*"), new TokenImpl("6")).evaluate(), 0.0001d);
        assertEquals(5d/6d, TokenParser.parseToExpression(new TokenImpl("5"), new TokenImpl("/"), new TokenImpl("6")).evaluate(), 0.0001d);
        assertEquals(30d + (5d/6d), TokenParser.parseToExpression(
                new TokenImpl("5"), new TokenImpl("/"), new TokenImpl("6"), new TokenImpl("+"),
                new TokenImpl("5"), new TokenImpl("*"), new TokenImpl("6")).evaluate(), 0.0001d);
        assertEquals(9d, TokenParser.parseToExpression(
                new TokenImpl("3"), new TokenImpl("*"), new TokenImpl("("), new TokenImpl("1"),
                new TokenImpl("+"), new TokenImpl("2")).evaluate(), 0.001d);
    }
}