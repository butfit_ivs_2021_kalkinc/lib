/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib.math;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.*;

public class LogarithmOperatorTest extends TestCase {

    private Expression expression0;
    private Expression expression1;
    private Expression expression2;
    private Expression expression3;
    private Expression expression4;
    private Expression expression16;
    private Expression expression256;
    private Expression expressionNeg3;
    private Expression expressionNeg4;
    private Expression expressionNeg16;
    private LogarithmOperator operator;

    @Before
    public void setUp() {
        expression0 = mock(Expression.class);
        expression1 = mock(Expression.class);
        expression2 = mock(Expression.class);
        expression3 = mock(Expression.class);
        expression4 = mock(Expression.class);
        expression16 = mock(Expression.class);
        expression256 = mock(Expression.class);
        expressionNeg3 = mock(Expression.class);
        expressionNeg4 = mock(Expression.class);
        expressionNeg16 = mock(Expression.class);
        when(expression0.evaluate()).thenReturn(0d);
        when(expression1.evaluate()).thenReturn(1d);
        when(expression2.evaluate()).thenReturn(2d);
        when(expression3.evaluate()).thenReturn(3d);
        when(expression4.evaluate()).thenReturn(4d);
        when(expression256.evaluate()).thenReturn(256d);
        when(expression16.evaluate()).thenReturn(16d);
        when(expressionNeg3.evaluate()).thenReturn(-3d);
        when(expressionNeg4.evaluate()).thenReturn(-4d);
        when(expressionNeg16.evaluate()).thenReturn(-16d);
        operator = new LogarithmOperator();
    }

    public void testNoExpressions() {
        assertEquals(operator.apply(new ArrayList<>()), 0d);
    }

    public void test1Expression() {
        assertEquals(operator.apply(Collections.singletonList(expression1)), 0d);
        assertEquals(operator.apply(Collections.singletonList(expression16)), 1.2041199826559248d);
        assertEquals(operator.apply(Collections.singletonList(expression256)), 2.4082399653118496d);
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Collections.singletonList(expressionNeg3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Collections.singletonList(expressionNeg16)));
    }

    public void test0Log0() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expression0)));
    }

    public void test0LogAnything() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expression1)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expression4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expression256)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expressionNeg4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expressionNeg16)));
    }

    public void testAnythingLog0() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression1, expression0)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression3, expression0)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg3, expression0)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg16, expression0)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression256, expression0)));
    }

    public void testPositiveLogPositive() {
        assertEquals(operator.apply(Arrays.asList(expression1, expression4)), Double.POSITIVE_INFINITY);
        assertEquals(operator.apply(Arrays.asList(expression3, expression4)), 1.2618595071429148d);
        assertEquals(operator.apply(Arrays.asList(expression4, expression1)), 0d);
        assertEquals(operator.apply(Arrays.asList(expression2, expression256)), 8d);
        assertEquals(operator.apply(Arrays.asList(expression4, expression3)), 0.7924812503605781d);
        assertEquals(operator.apply(Arrays.asList(expression4, expression4)), 1d);
        assertEquals(operator.apply(Arrays.asList(expression16, expression1)), 0d);
        assertEquals(operator.apply(Arrays.asList(expression16, expression4)), 0.5d);
    }

    public void testPositiveLogNegative() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression1, expressionNeg3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression3, expressionNeg4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression4, expressionNeg4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression16, expressionNeg3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression256, expressionNeg16)));
    }

    public void testNegativeLogPositive() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg3, expression256)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg3, expression1)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg4, expression256)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg16, expression4)));
    }

    public void testNegativeLogNegative() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg3, expressionNeg3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg3, expressionNeg4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg4, expressionNeg3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg16, expressionNeg4)));
    }

    public void test3Expressions() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expression1, expression4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg16, expression0, expressionNeg4)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression1, expressionNeg16, expressionNeg4)));
    }

    public void testEvenMoreExpressions() {
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expression0, expression1, expression4, expressionNeg3)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg16, expressionNeg4, expressionNeg3, expression0, expression256)));
        Assert.assertThrows(IllegalArgumentException.class, () -> operator.apply(Arrays.asList(expressionNeg3, expression256, expression4, expressionNeg4, expression4, expression3)));
    }
}