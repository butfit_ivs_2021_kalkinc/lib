/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib.math;

import java.util.List;

/**
 * An Operator implementation supporting the mathematical factorial operation.
 */
public class FactorialOperator implements Operator {
    /**
     * Calculates a factorial of a number according to the following rules:
     * if 0 expressions are passed in, returns the number 0
     * if 1 expression is passed in and if the expression is an integer or roughly an integer it returns the factorial of that expression
     * otherwise, throws an IllegalArgumentException or an IntegerExpressionExpectedException
     * @param expressions the list of the expressions
     * @return the factorial calculated as described above
     * @throws IllegalArgumentException when too many arguments are passed in, or when the factorial is negative number
     * @throws IntegerExpressionExpectedException when passed argument is not even roughly an integer
     */
    @Override
    public Double apply(List<Expression> expressions) {
        if (expressions.size() == 0) {
            return 0d;
        }
        else if (expressions.size() == 1) {
            double result = 1;
            Expression baseE = expressions.get(0);
            double base = baseE.evaluate();
            double roughlyInteger = base % 1;
            try {
                if ((!(0 <= roughlyInteger) || !(roughlyInteger < 0.05)) && (!(1 > roughlyInteger) || !(roughlyInteger > 0.95)))
                    throw new IntegerExpressionExpectedException("You should enter whole number, not decimal number!");
            } finally {
                base = Math.round(base);
            }
            if (base == 1d || base == 0d) {
                return result;
            }
            if (base < 0d) {
                throw new IllegalArgumentException("Entered number must be 0 or greater");
            }
            while (base >= 1d) {
                result = result * base;
                base--;
            }
            return result;
        }
        else {
            throw new IllegalArgumentException("You entered wrong number of Expressions!\nRight number is 1");
        }
    }
}