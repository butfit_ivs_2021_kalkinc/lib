/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib.math;

import java.util.List;
/**
 * An Operator implementation supporting the mathematical logarithm operation.
 */
public class LogarithmOperator implements Operator {
    /**
     * Calculates a logarithm of a number according to the following rules:
     * if 0 expressions are passed in, returns the number 0
     * if 1 expression is passed in, returns the common logarithm of that expression
     * if 2 expressions are passed in, returns the first expression's logarithm of the second expression
     * otherwise, throws an IllegalArgumentException
     * @param expressions the list of the expressions
     * @return the logarithm calculated as described above
     * @throws IllegalArgumentException when too many arguments are passed in, or when the logarithm can't be mathematically computed
     */
    @Override
    public Double apply(List<Expression> expressions) {
        if (expressions.size() == 0) {
            return 0d;
        }
        if (expressions.size() == 1) {
            double logarithm;
            Expression element = expressions.get(0);
            logarithm = element.evaluate();
            if (logarithm <= 0) {
                throw new IllegalArgumentException("Logarithm must be greater than 0");
            }
            else {
                return Math.log10(logarithm);
            }
        }
        if (expressions.size() == 2) {
            double base, logarithm;
            Expression element1 = expressions.get(0);
            Expression element2 = expressions.get(1);
            base = element1.evaluate();
            logarithm = element2.evaluate();
            if (logarithm <= 0 || base <= 0) {
                throw new IllegalArgumentException("Base and logarithm must be greater than 0");
            }
            return Math.log(logarithm)/Math.log(base);
        }
        else {
            throw new IllegalArgumentException("You entered wrong number of Expressions!\nRight number is 2");
        }
    }
}
