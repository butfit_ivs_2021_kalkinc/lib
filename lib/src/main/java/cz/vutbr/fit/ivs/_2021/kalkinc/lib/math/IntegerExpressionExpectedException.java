package cz.vutbr.fit.ivs._2021.kalkinc.lib.math;

public class IntegerExpressionExpectedException extends RuntimeException {
      public IntegerExpressionExpectedException(String message){
            super(message);
        }
    }