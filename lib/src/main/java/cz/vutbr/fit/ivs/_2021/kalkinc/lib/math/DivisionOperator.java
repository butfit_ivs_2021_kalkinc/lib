/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib.math;

import java.util.List;

/**
 * An Operator implementation supporting the mathematical division operation.
 */
public class DivisionOperator implements Operator {
    /**
     * Calculates a division of a number according to the following rules:
     * if 0 expressions are passed in, returns the number 0
     * if 1 expression is passed in, returns the passed expression
     * if 2 expressions are passed in, returns the first expression divided by second expression
     * otherwise, throws an IllegalArgumentException
     * @param expressions the list of the expressions
     * @return the division calculated as described above
     * @throws IllegalArgumentException when too many arguments are passed in
     */
    @Override
    public Double apply(List<Expression> expressions) {
        if (expressions.size() == 0) {
            return 0d;
        }
        if (expressions.size() == 1) {
            Expression element = expressions.get(0);
            return element.evaluate();
        }
        if (expressions.size() == 2) {
            return expressions.stream().mapToDouble(Expression::evaluate).reduce((a, b) -> a/b).getAsDouble();
        } else {
            throw new IllegalArgumentException("You entered wrong number of Expressions!\nRight number is 2");
        }
    }
}