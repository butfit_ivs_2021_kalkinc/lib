/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib;

import cz.vutbr.fit.ivs._2021.kalkinc.lib.math.Expression;
import cz.vutbr.fit.ivs._2021.kalkinc.lib.parser.ParserException;
import cz.vutbr.fit.ivs._2021.kalkinc.lib.parser.TokenParser;

import java.util.List;

/**
 * Calculator implementation
 */
public class CalculatorImpl implements Calculator {
    private double previousResult;
    private double memory;

    /**
     * Checks if some of the tokens are tokens, which works with memory or previous result and passes it to parser
     *
     * @param tokenList Tokens forming the expression.
     */
    @Override
    public void setExpression(List<Token> tokenList) throws ParserException {
        if (tokenList.size() == 1) {
            if (tokenList.get(0).getValue().equals("M+")) {
                memory += previousResult;
                previousResult = memory;
                return;
            } else if (tokenList.get(0).getValue().equals("M-")) {
                memory -= previousResult;
                previousResult = memory;
                return;
            }
        }
        if (tokenList.size() == 2) {
            if (tokenList.get(0).getValue().equals("M")) {
                if (tokenList.get(1).getValue().equals("M+")) {
                    memory *= 2;
                    previousResult = memory;
                    return;
                }
                if (tokenList.get(1).getValue().equals("M-")) {
                    memory = 0;
                    previousResult = memory;
                    return;
                }
            }
            if (tokenList.get(0).getValue().equals("Ans")) {
                if (tokenList.get(1).getValue().equals("M+")) {
                    memory += previousResult;
                    previousResult = memory;
                    return;
                }
                if (tokenList.get(1).getValue().equals("M-")) {
                    memory -= previousResult;
                    previousResult = memory;
                    return;
                }
            }
        }
        for (int i = 0; i < tokenList.size(); i++) {
            if (tokenList.get(i).getValue().equals("Ans")) {
                tokenList.remove(i);
                Token previousResultT = new TokenImpl(Double.toString(previousResult));
                tokenList.add(i, previousResultT);
            }
            if (tokenList.get(i).getValue().equals("M")) {
                tokenList.remove(i);
                Token memoryT = new TokenImpl(Double.toString(memory));
                tokenList.add(i, memoryT);
            }
        }
        Expression finalResult = TokenParser.parseToExpression(tokenList.toArray(new Token[0]));
        previousResult = finalResult.evaluate();
    }

    /**
     *
     * @return result
     */
    @Override
    public double getResult() {
        return previousResult;
    }
}
