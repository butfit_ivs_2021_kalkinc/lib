/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib;

import cz.vutbr.fit.ivs._2021.kalkinc.lib.parser.ParserException;

import java.util.List;

/**
 * Takes a mathematical expression as a {@link List} of separate {@link Token}s representing individual mathematical
 * symbols, or numbers. Internally parses this list and provides the numerical result of the expression.
 */
public interface Calculator {
    /**
     * Sets {@code tokenList} as currently entered expression.
     *
     * Expressions are entered as a {@link List} of individual {@link Token}s forming the expression
     * (numbers or mathematical symbols).
     * @param tokenList Tokens forming the expression.
     */
    void setExpression(List<Token> tokenList) throws ParserException;

    /**
     * Returns the calculated result of currently exntered expression.
     * @return The result
     */
    double getResult();
}
