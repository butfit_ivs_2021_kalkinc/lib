/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib.parser;

import cz.vutbr.fit.ivs._2021.kalkinc.lib.Token;
import cz.vutbr.fit.ivs._2021.kalkinc.lib.math.Expression;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * An object that may be either an Expression or a Token. To be used only internally by parser.
 */
class ExpressionToken {
    private final Expression expression;
    private final Token token;

    /**
     * @return Expression instance contained, or null
     */
    Expression getExpression() {
        return expression;
    }

    /**
     * @return Token instance contained, or null
     */
    Token getToken() {
        return token;
    }

    /**
     * @return True if contains an Expression
     */
    boolean isExpression() {
        return expression != null;
    }

    ExpressionToken(Expression expression, Token token) {
        this.expression = expression;
        this.token = token;
    }

    ExpressionToken(Token token) {
        this.token = token;
        this.expression = null;
    }

    ExpressionToken(Expression expression) {
        this.expression = expression;
        token = null;
    }
}
