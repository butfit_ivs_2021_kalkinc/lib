/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib.parser;

import cz.vutbr.fit.ivs._2021.kalkinc.lib.math.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class ExpressionTokenParser {
    private ExpressionTokenParser() {}

    /**
     * @brief Parses the parentheses-less input to an Expression.
     *
     * Operator precedence:
     * - Factorial
     * - Root, Power
     * - Logarithm
     * - Multiplication, Division
     * - Addition, Subtraction
     *
     * @internal practically untestable, except for fuzzy testing @endinternal
     * @param tokens Input list of ExpressionTokens
     * @return Parsed Expression
     */
    static Expression reduceExpressionTokenList(List<ExpressionToken> tokens) throws ParserException {
        if (tokens.size() < 2) {
            throw new IllegalArgumentException("Input list must contain more than one element.");
        }
        ArrayList<ExpressionToken> outList = new ArrayList<>();
        for (int i = tokens.size() - 1; i >= 0; i--) {
            if (!tokens.get(i).isExpression()) {
                if (tokens.get(i).getToken().getValue().equals("!")) {
                    if (i == 0 || !tokens.get(i-1).isExpression()) {
                        throw new ParserException("Invalid Factorial.");
                    }
                    i--;
                    outList.add(new ExpressionToken(new Subexpression(new FactorialOperator(), List.of(tokens.get(i).getExpression()))));
                    continue;
                }
            }
            outList.add(0, tokens.get(i));
        }
        for (int i = 0; i < outList.size(); i++) {
            if (!outList.get(i).isExpression()) {
                String operation = outList.get(i).getToken().getValue();
                if (operation.equals("root") || operation.equals("^")) {
                    if (i == 0 || i == outList.size() - 1 || !tokens.get(i - 1).isExpression() || !tokens.get(i + 1).isExpression()) {
                        throw new ParserException("Invalid Root/Power.");
                    }
                    i--;
                    Expression arg1 = outList.remove(i).getExpression();
                    outList.remove(i);
                    Expression arg2 = outList.remove(i).getExpression();
                    outList.add(i, new ExpressionToken(new Subexpression(
                            operation.equals("root") ? new RootOperator() : new PowerOperator()
                            , List.of(arg1, arg2))));
                }
            }
        }
        for (int i = 0; i < outList.size(); i++) {
            if (!outList.get(i).isExpression()) {
                String operation = outList.get(i).getToken().getValue();
                if (operation.equals("log")) {
                    if (i == 0 || i == outList.size() - 1 || !tokens.get(i - 1).isExpression() || !tokens.get(i + 1).isExpression()) {
                        throw new ParserException("Invalid Logarithm.");
                    }
                    i--;
                    Expression arg1 = outList.remove(i).getExpression();
                    outList.remove(i);
                    Expression arg2 = outList.remove(i).getExpression();
                    outList.add(i, new ExpressionToken(new Subexpression(
                            new LogarithmOperator()
                            , List.of(arg1, arg2))));
                }
            }
        }
        for (int i = 0; i < outList.size(); i++) {
            if (!outList.get(i).isExpression()) {
                String operation = outList.get(i).getToken().getValue();
                if (operation.equals("*") || operation.equals("/")) {
                    if (i == 0 || i == outList.size() - 1 || !tokens.get(i - 1).isExpression() || !tokens.get(i + 1).isExpression()) {
                        throw new ParserException("Invalid Multiplication/Division.");
                    }
                    i--;
                    Expression arg1 = outList.remove(i).getExpression();
                    outList.remove(i);
                    Expression arg2 = outList.remove(i).getExpression();
                    outList.add(i, new ExpressionToken(new Subexpression(
                            operation.equals("*") ? new MultiplicationOperator() : new DivisionOperator()
                            , List.of(arg1, arg2))));
                }
            }
        }
        for (int i = 0; i < outList.size(); i++) {
            if (!outList.get(i).isExpression()) {
                String operation = outList.get(i).getToken().getValue();
                if (operation.equals("+") || operation.equals("-")) {
                    if (i == 0 || i == outList.size() - 1 || !tokens.get(i - 1).isExpression() || !tokens.get(i + 1).isExpression()) {
                        throw new ParserException("Invalid Addition/Subtraction.");
                    }
                    i--;
                    Expression arg1 = outList.remove(i).getExpression();
                    outList.remove(i);
                    Expression arg2 = outList.remove(i).getExpression();
                    outList.add(i, new ExpressionToken(new Subexpression(
                            operation.equals("+") ? new AdditionOperator() : new SubtractionOperator()
                            , List.of(arg1, arg2))));
                }
            }
        }
        return outList.get(0).getExpression();
    }
}
