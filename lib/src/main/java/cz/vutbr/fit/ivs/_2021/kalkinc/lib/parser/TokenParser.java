/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cz.vutbr.fit.ivs._2021.kalkinc.lib.parser;

import cz.vutbr.fit.ivs._2021.kalkinc.lib.Token;
import cz.vutbr.fit.ivs._2021.kalkinc.lib.math.Expression;
import cz.vutbr.fit.ivs._2021.kalkinc.lib.math.SingleNumberExpression;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class TokenParser {
    private TokenParser() {}

    public static Expression parseToExpression(Token... tokens) throws ParserException {
        Stack<List<ExpressionToken>> expressionTokenListStack = new Stack<>();
        expressionTokenListStack.push(new ArrayList<>());
        for (Token token : tokens) {
            if (token.getValue().equals("(")) {
                expressionTokenListStack.push(new ArrayList<>());
                continue;
            }
            if (token.getValue().equals(")")) {
                Expression expression = ExpressionTokenParser.reduceExpressionTokenList(expressionTokenListStack.pop());
                expressionTokenListStack.peek().add(new ExpressionToken(expression));
                continue;
            }
            ExpressionToken et;
            try {
                et = new ExpressionToken(new SingleNumberExpression(Double.parseDouble(token.getValue())));
            } catch (NumberFormatException ex) {
                et = new ExpressionToken(token);
            }
            expressionTokenListStack.peek().add(et);
        }
        while (expressionTokenListStack.size() > 1) {
            Expression expression = ExpressionTokenParser.reduceExpressionTokenList(expressionTokenListStack.pop());
            expressionTokenListStack.peek().add(new ExpressionToken(expression));
        }
        return ExpressionTokenParser.reduceExpressionTokenList(expressionTokenListStack.pop());
    }
}
